# ReactNativeBUILD

This is a guide for build ReactNative Framework to chromium source .

## Setup 
- `cd ios` run `pod install`/`pod update`

## Usage
- run `./UniversalBuild.sh` to build Fat frameworks from pod

If build success, we'll have `Frameworks` folder, this folder contains single/fat build for each environment, we only need copy all framework of `Universal_Debug` to `src/ios/coccoc/thirdparty/reactnative/Debug` and `Universal_Release` to `src/ios/coccoc/thirdparty/reactnative/Release`

## Note
To add new framework from pod you have to follow these steps bellow
1. Add framework to pod file Ex:`pod 'Example'.`
2. Add framework name to `FRAMEWORK_NAMES` array in script `UniversalBuild.sh`.
3. Run pod 
4. Run script `./UniversalBuild.sh`

## Author
Ducdv
