# 1
# Set bash script to exit immediately if any commands fail.
set -e
PROJECT_NAME="ReactNativeBuild"
# release
xcodebuild BITCODE_GENERATION_MODE=bitcode OTHER_CFLAGS="-fembed-bitcode" -workspace "${PROJECT_NAME}.xcworkspace" -scheme "${PROJECT_NAME}" -configuration Release -arch arm64 only_active_arch=no defines_module=yes -sdk "iphoneos" -UseModernBuildSystem=NO
# release simulator
xcodebuild -workspace "${PROJECT_NAME}.xcworkspace" -scheme "${PROJECT_NAME}" -configuration Release -arch x86_64 only_active_arch=no defines_module=yes -sdk "iphonesimulator" -UseModernBuildSystem=NO
# debug iphone
xcodebuild -workspace "${PROJECT_NAME}.xcworkspace" -scheme "${PROJECT_NAME}" -configuration Debug -arch arm64 only_active_arch=no defines_module=yes -sdk "iphoneos" -UseModernBuildSystem=NO
# debug simulator
xcodebuild -workspace "${PROJECT_NAME}.xcworkspace" -scheme "${PROJECT_NAME}" -configuration Debug -arch x86_64 only_active_arch=no defines_module=yes -sdk "iphonesimulator" -UseModernBuildSystem=NO



# 2
# Setup some constants for use later on.
FRAMEWORK_NAMES="DoubleConversion FBReactNativeSpec fmt glog RCT-Folly RCTTypeSafety React-Core React-CoreModules React-cxxreact React-jsi React-jsiexecutor React-jsinspector React-logger React-perflogger React-RCTAnimation React-RCTBlob React-RCTImage React-RCTLinking React-RCTNetwork React-RCTSettings React-RCTText React-RCTVibration ReactCommon Yoga"
OUTPUT_DIR="${PWD}/Frameworks" # replace to ${SRCROOT} if copy script
DEBUG_PATH="Debug-iphonesimulator"
DEBUG_IPHONE="Debug-iphoneos"
RELEASE_PATH="Release-iphoneos"
RELEASE_SIMULATOR="Release-iphonesimulator"
FAT_PATH="Universal"
# Clean file if exists from previous run.
if [ -d "${OUTPUT_DIR}/" ]; then
rm -rf "${OUTPUT_DIR}/"
fi

TARGET_BUILD_DIR=`xcodebuild -workspace "${PROJECT_NAME}.xcworkspace" -scheme "${PROJECT_NAME}" -showBuildSettings | grep -m 1 "BUILT_PRODUCTS_DIR" | grep -oEi "\/.*"`

mkdir "${OUTPUT_DIR}"

for FRAMEWORK_NAME in ${FRAMEWORK_NAMES}; do
  echo ${FRAMEWORK_NAME};
  #copy simulator version
  cp -a "$TARGET_BUILD_DIR/../${DEBUG_PATH}/${FRAMEWORK_NAME}/." "${OUTPUT_DIR}/${DEBUG_PATH}/"
    #copy debug iphone version
  cp -a "$TARGET_BUILD_DIR/../${DEBUG_IPHONE}/${FRAMEWORK_NAME}/." "${OUTPUT_DIR}/${DEBUG_IPHONE}/"
  #copy release version
  cp -a "$TARGET_BUILD_DIR/../${RELEASE_PATH}/${FRAMEWORK_NAME}/." "${OUTPUT_DIR}/${RELEASE_PATH}/"
  cp -a "$TARGET_BUILD_DIR/../${RELEASE_SIMULATOR}/${FRAMEWORK_NAME}/." "${OUTPUT_DIR}/${RELEASE_SIMULATOR}/"
  
  #copy debug iphone version to fat
  cp -a "$TARGET_BUILD_DIR/../${DEBUG_IPHONE}/${FRAMEWORK_NAME}/." "${OUTPUT_DIR}/${FAT_PATH}_Debug/"
    #copy release iphone version to fat
  cp -a "$TARGET_BUILD_DIR/../${RELEASE_PATH}/${FRAMEWORK_NAME}/." "${OUTPUT_DIR}/${FAT_PATH}_Release/"
done

# Make a Fat binary
for FRAMEWORKS_DIR in "${OUTPUT_DIR}/${DEBUG_PATH}/*.framework"; do
#Find correct framework
 for FRAMEWORK_DIR in ${FRAMEWORKS_DIR}; do
  FRAMEWORK_NAME=$(basename $FRAMEWORK_DIR);
  BINARY_NAME=${FRAMEWORK_NAME%%.*};
# Make framework by lipo
  lipo -create -output "${OUTPUT_DIR}/${FAT_PATH}_Debug/${FRAMEWORK_NAME}/${BINARY_NAME}" "${OUTPUT_DIR}/${DEBUG_IPHONE}/${FRAMEWORK_NAME}/${BINARY_NAME}" "${OUTPUT_DIR}/${DEBUG_PATH}/${FRAMEWORK_NAME}/${BINARY_NAME}"
  lipo -create -output "${OUTPUT_DIR}/${FAT_PATH}_Release/${FRAMEWORK_NAME}/${BINARY_NAME}" "${OUTPUT_DIR}/${RELEASE_PATH}/${FRAMEWORK_NAME}/${BINARY_NAME}" "${OUTPUT_DIR}/${RELEASE_SIMULATOR}/${FRAMEWORK_NAME}/${BINARY_NAME}"
 done
done


