# ReactNativeBUILD

This is a guide for build ReactNative Framework to chromium source .

## Setup

1. Follow this link to setup enviroment https://reactnative.dev/docs/integration-with-existing-apps.
2. Config pod file
  Instead of use guide's Pod we use `our config Pod` > Copy/Replace content of Podfile in current directory to `ios/` then run `pod install`.
3. Build with bash script
- Create new `aggregate` target then change name to what ever you want Ex:`UniversalBuild`
- In `UniversalBuild` target, create new `Run Script` in `Build Phase` ( you can copy content from `UniversalBuild.sh` then paste to xCode(Recommend) or use add as `Input File` )
Note: In this step, you have to change value of `PROJECT_NAME` in script to `your project name`.  
- Build ( cmd+b )

## Usage
If build success, we'll have `Frameworks` folder, this folder contains single/fat build for each environment, we only need copy all framework of `Universal_Debug` to `src/ios/coccoc/thirdparty/reactnative/Debug` and `Universal_Release` to `src/ios/coccoc/thirdparty/reactnative/Release`

## Author
Ducdv
